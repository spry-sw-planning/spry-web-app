import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Loader } from 'semantic-ui-react';

// Redux
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from './data/reducers';
import { fetchSpryData } from './data/actions';
import SpryIndex from './data/spryIndex';

// Views
import SprintView from './modules/sprints/SprintView';
import ContributorsView from './modules/contributors/ContributorsView';
import TasksView from './modules/tasks/TasksView';
import ProjectsView from './modules/projects/ProjectsView';
import ProjectView from './modules/projects/ProjectView';
import NavView from './modules/nav/NavView';
import EmptyStateView from './modules/emptystate/EmptyStateView';


class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      initialized: false, // Tracks whether we have the initial data ready to start the app
      empty: true // Tracks whether we have at least one team member, project, and sprint
    };
    
    /**
     * Create the Redux store: http://redux.js.org/docs/basics/Store.html
     * The store will be used to store state for the client, to handle UI updates,
     * and the server, for the actual data. E.g.
     * {
     *   client: {...}
     *   remote: {...}
     * }
     */
    this.store = createStore(
      reducer,
      applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        createLogger() // neat middleware that logs actions
      )
    );
    
    this.index = new SpryIndex({store: this.store});
    this.store.subscribe(() => this.handleStoreStateChange());
    this.renderRoute = this.renderRoute.bind(this);
    
    // Fetch the data from the remote location
    this.store.dispatch(fetchSpryData());
  }
  
  /**
   * Handles changes to the Redux store.
   */
  handleStoreStateChange() {
    // If the data has been downloaded, hide the spinner and show the app UI
    if (this.store.getState().client.dataIndexed) {
      this.setState({initialized: true, empty: this.index.isEmpty()});
    }
  }
  
  renderRoute = routeProps => {
    const viewName = routeProps.match.params.viewName || 'plan';
    const id = routeProps.match.params.id || '';
    switch (viewName) {
      case "projects":
        return (
          <div>
            <NavView activeItem={viewName} showNavItems />
            <div className='main-content'><ProjectsView spryIndex={this.index} store={this.store}/></div>
          </div>
        );
      case "project":
        return (
          <div>
            <NavView activeItem={viewName} showNavItems />
            <div className='main-content'><ProjectView spryIndex={this.index} store={this.store} projectId={id}/></div>
          </div>
        );
      case "contributors":
        return (
          <div>
            <NavView activeItem={viewName} showNavItems />
            <div className='main-content'><ContributorsView store={this.store}/></div>
          </div>
        );
      case "tasks":
        return (
          <div>
            <NavView activeItem={viewName} showNavItems />
            <div className='main-content'><TasksView spryIndex={this.index} store={this.store}/></div>
          </div>
        );
      default:
        return (
          <div>
            <NavView activeItem={viewName} showNavItems />
            <div className='main-content'><SprintView spryIndex={this.index} store={this.store}/></div>
          </div>
        );
    }
  };
  
  render() {
    if (this.state.initialized) {
      
      if (!this.state.empty) {
        return (
          <Router>
            <div>
              <Route exact path="/" render={props => this.renderRoute(props)}/>
              <Route path="/:viewName/:id?" render={props => this.renderRoute(props)}/>
            </div>
          </Router>
        );
        
      } else {
        return <div><EmptyStateView spryIndex={this.index} store={this.store} /></div>;
      }
    } else {
      // Show a spinner if we're still waiting for the data to load
      return (<div><Loader active /></div>);
    }
  }
}

export default App;
