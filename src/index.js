import React from 'react';
import ReactDOM from 'react-dom';

// Views
import App from './App';

// Styles
import 'semantic-ui-css/semantic.min.css';
import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
