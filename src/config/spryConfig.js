const CONTRIBUTOR_ROLES = [
  { id: 'PM', name: 'PM' },
  { id: 'SDE', name: 'SDE' },
  { id: 'DESIGNER', name: 'Designer' },
  { id: 'TPM', name: 'TPM' },
  { id: 'SDM', name: 'SDM' },
  { id: 'QA', name: 'QA' },
  { id: 'QAM', name: 'QAM' }
];

const CONTRIBUTOR_SKILLS = [
  { id: 'IOS', name: 'iOS' },
  { id: 'ANDROID', name: 'Android' },
  { id: 'UI', name: 'UI' },
  { id: 'SERVICES', name: 'Services' },
  { id: 'RN', name: 'React Native' },
  { id: 'WEB', name: 'Web' },
  { id: 'CV', name: 'CV' },
  { id: 'ML', name: 'ML' },
  { id: 'EMBEDDED', name: 'Embedded SW' }
];

const PROJECT_TYPES = [
  { id: 'feature', name: 'Feature', icon: 'add square' },
  { id: 'enhancement', name: 'Enhancement', icon: 'selected radio' },
  { id: 'engineering_excellence', name: 'Engineering Excellence', icon: 'browser' },
  { id: 'operational_excellence', name: 'Operational Excellence', icon: 'area chart' },
];

export function getContributorRoles() {
  return CONTRIBUTOR_ROLES;
}

export function getContributorSkills() {
  return CONTRIBUTOR_SKILLS;
}

export function getProjectTypes() {
  return PROJECT_TYPES;
}
