import React from 'react';
import {Button, Container, Header, Icon, Modal} from 'semantic-ui-react';
import _ from 'lodash';
import { getProjectTypes } from '../../config/spryConfig';
import AddTaskToProjectCreateUpdateView from './AddTaskToProjectCreateUpdateView';

class ProjectView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      column: null,
      data: this.props.spryIndex.sortedProjects[this.props.projectId],
      direction: null,
    };

    this.props.store.subscribe(() => this.handleStoreStateChange());
  }

  handleStoreStateChange() {
    this.setState({ ...this.state, data: this.props.spryIndex.sortedProjects[this.props.projectId] });
  }

  renderProject() {
    const { name, description, leads, jiraLink, type, startDate, tasks } = this.state.data;
    const project = getProjectTypes().find(project => {
      if (project.id === type) return project;
    });
    const icon = project.icon;

    return (
      <div>
        <Container>
          <Header size='huge'>
            <Header.Content>
              <Icon name={icon} size='small' /> {name}
            </Header.Content>
            <Header.Subheader>{description}</Header.Subheader>
          </Header>

          <Header>
            Leads
            <Header.Subheader>{leads}</Header.Subheader>
          </Header>

          <Header>
            Start Date
            <Header.Subheader>{startDate}</Header.Subheader>
          </Header>

          <Header>
            Jira
            <Header.Subheader>{jiraLink}</Header.Subheader>
          </Header>

          <Header>
            Tasks
            {_.map(tasks, ({ title, leads }, key) => (
              <Header.Subheader>{title}</Header.Subheader>
            ))}
            <Button icon='plus' content='Add tasks' color='blue' onClick={() => {this.createView.open();}} />

            <AddTaskToProjectCreateUpdateView store={this.props.store} spryIndex={this.props.spryIndex} projectId={this.props.projectId} ref={el => { this.createView = el }} />
          </Header>
        </Container>
      </div>
    );
  }

  renderProjectNotFound() {
    return (
      <div>
        <Container>
          <Header>Project {this.props.project} not found</Header>
        </Container>
      </div>
    )
  }

  render() {
    if (!this.state.data) {
      return this.renderProjectNotFound();
    }

    return this.renderProject();
  }
}

export default ProjectView;
