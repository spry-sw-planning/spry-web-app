import { combineReducers } from 'redux';
import {
  CREATE_SPRINT, CREATE_CONTRIBUTOR, CREATE_TASK, CREATE_PROJECT, RECEIVE_SPRY_DATA, INDEX_SPRY_DATA,
  ADD_TASKS_TO_PROJECT
} from './actions';

/**
 * Handles updates to the data used by the client.
 */
function clientReducer(state = {}, action) {
  switch (action.type) {
    case RECEIVE_SPRY_DATA:
    case CREATE_SPRINT:
    case CREATE_CONTRIBUTOR:
    case CREATE_TASK:
    case CREATE_PROJECT:
    case ADD_TASKS_TO_PROJECT:
      return {dataReceived: true, dataIndexed: false};
    case INDEX_SPRY_DATA:
      return {...state, dataIndexed: true};
    default:
      return state;
  }
}

/**
 * Handles updates to the Spry data.
 */
function spryReducer(state = {}, action) {
  switch (action.type) {
    case RECEIVE_SPRY_DATA:
      return action.spry;
    case CREATE_SPRINT:
      return createSprint(state, action.sprint);
    case CREATE_CONTRIBUTOR:
      return createContributor(state, action.contributor);
    case CREATE_TASK:
      return createTask(state, action.task);
    case CREATE_PROJECT:
      return createProject(state, action.project);
    case ADD_TASKS_TO_PROJECT:
      return addTasksToProject(state, action.tasks, action.projectId);
    default:
      return state;
  }
}

function createSprint(state, sprint) {
  let newState = Object.assign({}, state);
  if (!newState.sprints) {
    newState.sprints = {};
  }
  newState.sprints[sprint.id] = sprint;
  
  return newState;
}

function createContributor(state, contributor) {
  let newState = Object.assign({}, state);
  if (!newState.contributors) {
    newState.contributors = {};
  }
  newState.contributors[contributor.alias] = contributor;
  
  return newState;
}

function createTask(state, task) {
  let newState = Object.assign({}, state);
  if (!newState.tasks) {
    newState.tasks = {};
  }

  let taskId = Object.keys(newState.tasks).length;
  newState.tasks[taskId] = task;

  return newState;
}

function createProject(state, project) {
  let newState = Object.assign({}, state);
  if (!newState.projects) {
    newState.projects = {};
  }

  let projectId = Object.keys(newState.projects).length;
  newState.projects[projectId] = project;

  return newState;
}

function addTasksToProject(state, tasks, projectId) {
  let newState = Object.assign({}, state);

  for (let i = 0; i < tasks.length; i++) {
    let task = newState.tasks[tasks[i]];
    task.project = projectId;
  }

  return newState;
}

export default combineReducers({
  client: clientReducer,
  spry: spryReducer
});