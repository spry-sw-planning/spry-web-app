import validator from 'validator';

/**
 * A validator class with fluid methods to chain validation calls.
 */
class Validator {
    
  constructor(props) {
    this.result = {
        hasErrors: false,
        fields: {},
    }      
  }
  
  validateAlias(key, value) {
    this.result.fields[key] = !(value && validator.isLength(value.toString().trim(), {min: 3}));
    return this;
  }
    
  validateName(key, value) {
    this.result.fields[key] = !(value && validator.isLength(value.toString().trim(), {min: 2}));
    return this;
  }

  validateDate(key, value) {
    this.result.fields[key] = !(value && validator.isISO8601(value));
    return this;
  }
  
  validateId(key, value) {
    this.result.fields[key] = !(value && validator.isLength(value.toString().trim(), {min: 5}));
    return this;
  }
  
  validateUniqueness(key, value, getObject) {
    if (this.result.fields[key]) return this;
    this.result.fields[key] = getObject(value) !== null;
    return this;
  }
  
  validateNotBlank(key, value) {
    this.result.fields[key] = !value || value.toString().trim().length === 0;
    return this;
  }

  getResult() {
      for (let key in this.result.fields) {
        if (this.result.fields[key]) {
          this.result.hasErrors = true;
          break;
        }
      }
      return this.result;
  }
}

export default Validator;

