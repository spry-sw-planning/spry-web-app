# About
Spry is a simple web app for software development planning. It's ideal for teams that want a lightweight Scrum management tool.
You can use Spry to define your team's resources, skillsets, roles, projects, sprint tasks, and dependencies.
It automatically generates a schedule based on work estimates, allowing you to better visualize and share your software delivery schedule.

